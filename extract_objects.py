from __future__ import print_function
import os
import time
import cv2
import pandas as pd
import numpy as np
import multiprocessing

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def crop_pseudo(session='1BM1', coord_csv = 'coord.csv', outdir = 'test', width=1500,height=750,buffer=500):

    print(session)

    df = pd.read_csv(coord_csv)
    df = df[df.session == session]

    # get coord for this session
    y_top = int(df['y_top'].iloc[0]) * 10
    x_left = int(df['x_left'].iloc[0]) * 10

    y_top -= buffer
    x_left -= buffer

    if(y_top < 0):
        y_top = 0
    if(x_left < 0):
        x_left = 0


    if df['dir'].iloc[0] == 'h':
        x_right = x_left + width + buffer
        y_bottom = y_top + height + buffer
    
    if df['dir'].iloc[0] == 'v':
        x_right = x_left + height + buffer
        y_bottom = y_top + width + buffer
    
    if(y_bottom > 3000): # /!\ hardcoded
        y_bottom = 3000
    if(x_right > 4000): # /!\ hardcoded
        x_right = 4000
    
    session_path = 'to_crop/'+session
    # get all images from this session
    all_images = sorted([os.path.join(root, name)
            for root, dirs, files in os.walk(session_path)
            for name in files
            if name.endswith((".JPG"))])
    
    #print(all_images)
    n=len(all_images)
    i=0
    #print("%d, %d, %d, %d" %(x_left,x_right,y_top,y_bottom))

    # crop all
    for image_path in all_images:
        i+=1
        print("%s %d/%d" %(image_path,i,n))
        image0 = cv2.imread(image_path, 1)  # color
        
        window = image0[y_top:y_bottom, x_left:x_right] # crop de 0 octets ?
        
        new_path = "/".join(image_path.strip("/").split('/')[1:])

        outpath = os.path.join(outdir, new_path)
        # print(outpath)
        cv2.imwrite(outpath, window)

        if i % 100 == 0:
            time.sleep(1)


def crop_multi(target_dir = 'to_crop'):

    t0 = time.time()

    all_dirs = sorted([root for root, dirs, files in os.walk(target_dir)])

    for dir in all_dirs:

        path = "/".join(dir.strip("/").split('/')[1:])
        outpath = 'test/' + path
        create_dir(outpath)

    cpu_num = int(multiprocessing.cpu_count()) - 2
    
    # chunked_list = chunkIt(os.listdir(target_dir), cpu_num)

    # manager = multiprocessing.Manager()
    
    session_list = sorted(os.listdir(target_dir))

    # firsts = session_list[0:cpu_num]
    # firsts = session_list[cpu_num:len(session_list)]
    firsts = session_list

    print(firsts)

    time.sleep(25)


    procs = []


    proc_num=0

    for session in firsts:

        print(session)
        
        proc_num +=1
        # creating processes 
        proc = multiprocessing.Process(target=crop_pseudo, args=(session,'coord.csv','test',1500,750,500)) 
        procs.append(proc)
        proc.start()
  
    # starting processes
    for proc in procs:
        
        proc.join() 

    t1 = time.time()

    print(t1-t0 - 625)

  
    # # loop through images with detection
    # session_path = 'to_crop/'+ session
    # print(os.listdir(session_path))
    # test_images_list = [f for f in os.listdir(session_path) if (f.endswith('.jpg') or f.endswith('.JPG'))]
    # print(session_path)
    # print(test_images_list)

    

    # # all processes finished 
    # print("crop done!") 


crop_multi()